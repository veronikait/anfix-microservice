package edu.anfix.microservicio.client;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import edu.anfix.microservicio.model.Player;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PlayersClientImpl {

	public Player getPlayer() {

		String fooResourceUrl = "https://jsonplaceholder.typicode.com/posts/1";
		
		RestTemplate restTemplate = new RestTemplate();
		Player player = restTemplate.getForObject(fooResourceUrl, Player.class);
		
		
		
		log.info("player  {}", player);

		return player;

	}

}

