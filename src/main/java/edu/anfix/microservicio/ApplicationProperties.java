package edu.anfix.microservicio;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Data
@Configuration
public class ApplicationProperties {

	@Value("${app.name}")
	private String appName;
	
	@Value("${app.edition}")
	private String edition;

	@Value("${app.year}")
	private int year;
	
	
}
