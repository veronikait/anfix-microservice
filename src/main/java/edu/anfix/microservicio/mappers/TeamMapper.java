package edu.anfix.microservicio.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import edu.anfix.microservicio.dao.entities.TeamEntity;
import edu.anfix.microservicio.model.Team;

@Mapper(componentModel = "spring")
public interface TeamMapper {

	@Mapping(source = "team.teamName",target = "name")
	public TeamEntity toTeamEntity(Team team);

	@Mapping(source = "teamEntity.name",target = "teamName")
	public Team toTeam(TeamEntity teamEntity);
	
	public List<Team> toTeams(List<TeamEntity> teamEntities);

	public List<TeamEntity> toTeamEntities(List<Team> teamEntities);

}
