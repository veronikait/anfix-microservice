package edu.anfix.microservicio.controllers;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.anfix.microservicio.ApplicationProperties;
import edu.anfix.microservicio.model.Team;
import edu.anfix.microservicio.model.Tournament;
import edu.anfix.microservicio.services.BaseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/team")
@Api(tags = "Rest team Api")
@Validated
@Slf4j
public class TeamController {
	
	//Facade Slf4
	//Se entendio? :) contesten por chat si pudieron entender
	//Usando lombok podemos incluso simplificafr el codigo de look up.
	//private static Logger log = LoggerFactory.getLogger(TeamController.class);

	
	@Autowired
	private BaseService<Team> teamService;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private ApplicationProperties applicationProperties;

	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(notes = "${rest.api.team.operation.get_by_id}", value = "get one team")
	public ResponseEntity<Team> getById(
			@ApiParam(required = true, example = "1", allowableValues = "1,2,3,4,5") @PathVariable("id") Integer id) {

		//Mas performante que utilizar el operador "+" ya
		//que si no se usa la traza no se ocupa tiempo resolviendo la conjuncion +
		log.info("Get one team by id {}",id);
		
		try {
			
			log.debug("Get Team by id full detail");

			log.info("return one team");
			return ResponseEntity.ok(teamService.getById(id).orElseThrow());
			

		} catch (NoSuchElementException e) {

			log.error("Error at try to recover one team by id {}", id);
			log.error(e.getMessage());
			
			return ResponseEntity.notFound().build();

		}

		
	}

	@GetMapping("/{id}/tournaments")
	public ResponseEntity<List<Tournament>> getAllTournamentByTeam(@PathVariable("id") Integer teamId) {

		List<Tournament> tournaments = new ArrayList<>();
		tournaments.add(new Tournament(1, "Copa del rey"));
		tournaments.add(new Tournament(2, "La liga"));
		tournaments.add(new Tournament(3, "Champion"));

		return ResponseEntity.ok(tournaments);
	}

	@GetMapping("/")
	public ResponseEntity<CollectionModel<Team>> getAll() {

		System.out.println(applicationProperties.getAppName());
		System.out.println(applicationProperties.getEdition());
		System.out.println(applicationProperties.getYear());
		System.out.println(messageSource.getMessage("app.title", null, new Locale("es", "ES")));

		List<Team> teams = teamService.listAll();

		for (Team team : teams) {

			Link link = linkTo(methodOn(TeamController.class).getById(team.getId())).withSelfRel().withType("GET");
			Link linkTournament = linkTo(methodOn(TeamController.class).getAllTournamentByTeam(team.getId()))
					.withRel("listTournaments").withType("GET");

			team.add(link);
			team.add(linkTournament);
		}

		Link linkTeams = linkTo(methodOn(TeamController.class).getAll()).withSelfRel().withType("GET");
		CollectionModel<Team> collection = new CollectionModel<>(teams, linkTeams);

		return ResponseEntity.ok(collection);
	}

	@PostMapping
	public ResponseEntity<Team> create(@Valid @RequestBody Team team) {

		// creacion
		Integer id = teamService.save(team);
		System.out.println(id);

		return new ResponseEntity<Team>(team, HttpStatus.CREATED);
	}

	@PutMapping
	public ResponseEntity<Team> update(@RequestBody Team team) {

		try {

			team = teamService.getById(team.getId()).orElseThrow();
			team = teamService.update(team);
			return ResponseEntity.ok(team);

		} catch (NoSuchElementException e) {
			return ResponseEntity.notFound().build();
		}

	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteById(@PathVariable("id") Integer id) {

		try {

			teamService.getById(id).orElseThrow();
			teamService.deleteById(id);
			return new ResponseEntity<Void>(HttpStatus.OK);

		} catch (NoSuchElementException e) {
			return ResponseEntity.notFound().build();
		}

	}

}
