package edu.anfix.microservicio.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.anfix.microservicio.model.Match;
import edu.anfix.microservicio.services.MatchService;

@RestController
@RequestMapping("/match")
public class MatchController {
	
	@Autowired
	private MatchService matchService;

	@PostMapping("/play")
	public ResponseEntity<Match> play(@RequestBody Match match) {
		
		matchService.play(match);

		return ResponseEntity.ok(match);

	}

}
