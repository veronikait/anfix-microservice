package edu.anfix.microservicio.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
	
	@GetMapping("/hola")
	public ResponseEntity<String> holaMundo() {
		
		//return new ResponseEntity<String>("hola mundo spring", HttpStatus.OK);
		return ResponseEntity.ok("Hola Mundo Spring Boot");
	}
	
}
