package edu.anfix.microservicio.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import edu.anfix.microservicio.model.Player;
import edu.anfix.microservicio.services.impl.PlayerServiceImpl;

@RestController
@RequestMapping("/player")
public class PlayerController {

	@Autowired
	private PlayerServiceImpl playerService;

	@GetMapping
	public ResponseEntity<List<Player>> listAll() {

		return ResponseEntity.ok(playerService.listAll());

	}

}
