package edu.anfix.microservicio.services;

import java.util.List;
import java.util.Optional;

public interface BaseService<T> {
	
	public Optional<T> getById(Integer id);
	
	public List<T> listAll();
	
	public Integer save(T obj);
	
	public T update(T obj);
	
	public boolean deleteById(Integer id);
}
