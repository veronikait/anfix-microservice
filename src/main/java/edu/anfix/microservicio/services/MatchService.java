package edu.anfix.microservicio.services;

import edu.anfix.microservicio.model.Match;

public interface MatchService {
	
	public void play(Match match);

}
