package edu.anfix.microservicio.services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import edu.anfix.microservicio.dao.repositories.PlayerMongoRepository;
import edu.anfix.microservicio.model.Player;
import edu.anfix.microservicio.services.BaseService;

@Lazy
@Primary
@Service("PlayerImpl")
public class PlayerServiceImpl implements BaseService<Player>{
	
	@Autowired
	private PlayerMongoRepository playerRepository;

	@Override
	public Optional<Player> getById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Player> listAll() {
		return playerRepository.findAll();
	}

	@Override
	public Integer save(Player obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Player update(Player obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteById(Integer id) {
		// TODO Auto-generated method stub
		return false;
	}

}
