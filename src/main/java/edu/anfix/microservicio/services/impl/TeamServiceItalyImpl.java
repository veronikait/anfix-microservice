package edu.anfix.microservicio.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import edu.anfix.microservicio.model.Team;
import edu.anfix.microservicio.services.BaseService;

@Lazy
@Primary
@Service("itaImpl")
@ConditionalOnProperty(value = "app.country",havingValue = "it")
public class TeamServiceItalyImpl implements BaseService<Team>{

	@Override
	public Optional<Team> getById(Integer id) {
		
		Optional<Team> optTeam = Optional.ofNullable(new Team(1,"Milan"));
		
		return optTeam;
	}

	@Override
	public List<Team> listAll() {

		List<Team> teams = new ArrayList<>();

		Team team = new Team(1, "Juventus");
		Team team2 = new Team(2, "Inter");
		Team team3 = new Team(3, "Fiorentina");

		teams.add(team);
		teams.add(team2);
		teams.add(team3);
		
		return teams;
	}

	@Override
	public Integer save(Team team) {

		//mucho codigo
		
		return team.getId();
	}

	@Override
	public Team update(Team team) {
		return team;
	}

	@Override
	public boolean deleteById(Integer id) {
		return true;
	}

}
