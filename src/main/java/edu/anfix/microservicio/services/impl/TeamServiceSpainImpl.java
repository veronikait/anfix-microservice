package edu.anfix.microservicio.services.impl;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;

import edu.anfix.microservicio.dao.entities.TeamEntity;
import edu.anfix.microservicio.dao.repositories.TeamRepository;
import edu.anfix.microservicio.mappers.TeamMapper;
import edu.anfix.microservicio.model.Team;
import edu.anfix.microservicio.services.BaseService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Lazy
@RequiredArgsConstructor
@Service("espImpl")
@ConditionalOnProperty(value = "app.country", havingValue = "es")
public class TeamServiceSpainImpl implements BaseService<Team> {

	@NonNull
	private TeamRepository teamRepository;
	
	@NonNull
	private TeamMapper teamMapper;
	
	@Autowired
	private HttpServletRequest request;

	@Override
	public Optional<Team> getById(Integer id) {

		Optional<TeamEntity> optTeam = teamRepository.findById(id);
		
		if (optTeam.isPresent()) {
			
			TeamEntity teamEntity = optTeam.get();
			Team team = teamMapper.toTeam(teamEntity);
			return Optional.ofNullable(team);
			
			
		}
		
		return Optional.empty();
	}

	@Override
	public List<Team> listAll() {

		//Integer size = Integer.parseInt(request.getParameter("size"));
		//Integer page = Integer.parseInt(request.getParameter("page"));
		
		//Muchas reglas de negocio	
		List<TeamEntity> teamEntities = teamRepository.findAll();
		List<Team> teams = teamMapper.toTeams(teamEntities);
	
		return teams;
	}

	@Override
	public Integer save(Team team) {

		// mucho codigo
		TeamEntity teamEntity = teamMapper.toTeamEntity(team);
		teamRepository.save(teamEntity);
		return team.getId();
	}

	@Override
	public Team update(Team team) {
		save(team);
		return team;
	}

	@Override
	public boolean deleteById(Integer id) {
		teamRepository.deleteById(id);
		return true;
	}

}
