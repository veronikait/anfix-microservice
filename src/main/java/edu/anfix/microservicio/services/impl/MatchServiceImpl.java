package edu.anfix.microservicio.services.impl;

import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.anfix.microservicio.client.PlayersClientImpl;
import edu.anfix.microservicio.model.Match;
import edu.anfix.microservicio.model.Player;
import edu.anfix.microservicio.services.MatchService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MatchServiceImpl implements MatchService {

	@Autowired
	private PlayersClientImpl playerClient;

	@Override
	public void play(Match match) {

		MDC.put("estadio", match.getStadium().getName());

		Player player = playerClient.getPlayer();

		log.info(player.getTitle());

		log.info("Begin match");

		for (int i = 0; i < 90; i++) {

			log.info("minute " + i);
		}

		log.info("Finish atch");
	}

}
