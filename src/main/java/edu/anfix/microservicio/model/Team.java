package edu.anfix.microservicio.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.hateoas.RepresentationModel;

import edu.anfix.microservicio.model.validators.CIF;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor 
@ApiModel(description = "Team Football Model")
public class Team extends RepresentationModel<Team> {

	@NonNull
	private Integer id;

	@NonNull
	@Size(min = 7, max = 10)
	@NotNull(message = "Cannot be null")
	@NotBlank(message = "Cannot be blank")
	@ApiModelProperty(allowableValues = "Real Madrid, Barcelona, Milan", notes = "Name of football team")
	private String teamName;

	@CIF
	private String cifFiscal;

	@Min(1870)
	@Max(2020)
	private int year;

	private Set<Player> players;

}
