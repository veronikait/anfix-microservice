package edu.anfix.microservicio.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Referee {

	private String name;
	
	private boolean international;
	
	private int quantityMatches;
	
	
}
