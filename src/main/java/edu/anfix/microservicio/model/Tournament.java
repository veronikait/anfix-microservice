package edu.anfix.microservicio.model;

import java.util.Set;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class Tournament {

	@NonNull
	private int id;

	@NonNull
	private String name;

	private Set<MatchDate> matchDates;

}
