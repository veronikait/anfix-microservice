package edu.anfix.microservicio.model;

import java.util.Set;

import lombok.Data;

@Data
public class MatchDate {
	
	private int dateNumber;
	
	private Set<Match> matches;

}
