package edu.anfix.microservicio.model;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document("players")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Player {

	@MongoId
	private String id;
	
	private String name;
	
	private String title;
	
	private Integer number;
	
	private boolean capitan;
	
}
