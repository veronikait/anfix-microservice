package edu.anfix.microservicio.model;

import lombok.Data;

@Data
public class Match {

	private Stadium stadium;

	public void generateReferee() {

		Referee referee = Referee.builder()
							.name("Pierluigi Colina")
							.quantityMatches(1000)
							.international(true)
							.build();

	}

}
