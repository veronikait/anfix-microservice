package edu.anfix.microservicio.dao.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import edu.anfix.microservicio.model.Player;

@Repository
public interface PlayerMongoRepository extends MongoRepository<Player, String>{

}

