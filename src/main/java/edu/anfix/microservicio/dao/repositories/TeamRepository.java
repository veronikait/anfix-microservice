package edu.anfix.microservicio.dao.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import edu.anfix.microservicio.dao.entities.TeamEntity;

@Repository
public interface TeamRepository extends JpaRepository<TeamEntity, Integer> {

	public List<TeamEntity> findByYearLessThan(int year);

	public List<TeamEntity> findByNameLike(String name);
	
	@Query(value="select * from teams where name = ?1 and year >= ?2 and year <= ?3" ,nativeQuery = true)
	public List<TeamEntity> findAllTeamsBetweenYearsAndName(String name,int yearBegin,int yearEnd);

}
