package edu.anfix.microservicio.dao.repositories;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import edu.anfix.microservicio.dao.entities.TeamEntity;

@DataJpaTest
public class TeamRepositoryIntegrationTest {

	@Autowired
	private TeamRepository teamRepostory;

	@Test
	public void WhenFindById_thenReturnTeam() {

		TeamEntity team = new TeamEntity(1, "AC Milan",1990);
		teamRepostory.save(team);

		// AssertJ
		Assertions.assertThat(teamRepostory.findById(1))
										.isNotEmpty()
										.hasValue(team);
	}

}
