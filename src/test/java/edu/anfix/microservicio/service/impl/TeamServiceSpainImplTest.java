package edu.anfix.microservicio.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;

import edu.anfix.microservicio.dao.entities.TeamEntity;
import edu.anfix.microservicio.dao.repositories.TeamRepository;
import edu.anfix.microservicio.mappers.TeamMapper;
import edu.anfix.microservicio.model.Team;
import edu.anfix.microservicio.services.impl.TeamServiceSpainImpl;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;

import lombok.extern.slf4j.Slf4j;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;

@Slf4j
@DisplayName("Test unitario especifico para la prueba de CRUD de equipos de la Liga")
public class TeamServiceSpainImplTest {

	private PodamFactory factory;

	@InjectMocks
	private TeamServiceSpainImpl teamServiceSpainImpl;

	@Mock
	private TeamRepository teamRepositoryMock;

	@Spy
	private TeamMapper teamMapperMock;

	@BeforeAll
	static void setup() {
		log.info("@BeforeAll - executes once before all test methods in this class");
	}

	@BeforeEach
	public void setUpBefore() {

		MockitoAnnotations.initMocks(this);

		factory = new PodamFactoryImpl();
		factory.getStrategy().setMemoization(false);

		log.info("Build Mock for teamRepository");

		List<TeamEntity> teamsEntity = new ArrayList<>();

		for (int i = 0; i < 10; i++) {
			TeamEntity teamEntity = factory.manufacturePojo(TeamEntity.class);
			teamsEntity.add(teamEntity);
		}

		Mockito.when(teamRepositoryMock.findAll()).thenReturn(teamsEntity);

		log.info("Build Mock for teamMapper");

		List<Team> teams = new ArrayList<>();

		for (int i = 0; i < 10; i++) {
			Team team = factory.manufacturePojoWithFullData(Team.class);
			teams.add(team);

		}
		
		teams.forEach(t -> log.info("{}",t));

		Mockito.when(teamMapperMock.toTeams(teamsEntity)).thenReturn(teams);

	}

	@Order(1)
	@Test
	@EnabledOnOs(OS.WINDOWS)
	@DisplayName("Probando el retorno de la lista de equipos de primera division de España")
	final void givenTeams_whenGetTeams_thenReturnAllTeams() {

		List<Team> teams = teamServiceSpainImpl.listAll();

		// Assertions
		assertNotNull(teams);
		assertTrue(teams.size() > 0);
		Team team = teams.get(0);
		log.info("Team: " + team.getTeamName());

		Mockito.verify(teamRepositoryMock, Mockito.atLeast(1)).findAll();
		Mockito.verify(teamRepositoryMock, Mockito.never()).delete(Mockito.any(TeamEntity.class));

	}

	@ParameterizedTest
	@ValueSource(ints = { 1, 2, 3 })
	public final void sum(int i) {

		log.info("valor i " + i);
		assertTrue(i > 0);
	}

	@AfterEach
	void tearDown() {
		log.info("@AfterEach - executed after each test method.");
	}

	@AfterAll
	static void done() {
		log.info("@AfterAll - executed after all test methods.");

	}
}